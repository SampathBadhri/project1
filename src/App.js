import React from "react";
import Home from './Components/HomePage/Home';
import Navbar from './Components/Navbar/Navbar';



const App = () => {
    return(
        <main>
            <Navbar />
            <Home />
        </main>
    )
}

export default App;