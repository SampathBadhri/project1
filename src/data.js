const data = {
    baseURL: "https://v6.exchangerate-api.com/v6/31b3d620a96669075780fd70/latest/USD",
    products: [{
        id: 1,
        brand: "Adidas",
        product_title: "Running Shoes",
        product_img: "https://icartadmin.irax.in/assets/img/uploads/products/shoes1.webp",
        product_price: 15
    },{
        id: 2,
        brand: "Reebok",
        product_title: "Running Shoes",
        product_img: "https://icartadmin.irax.in/assets/img/uploads/products/shoes2.webp",
        product_price: 20
    },{
        id: 3,
        brand: "Neutrogena",
        product_title: "Ultra Sheer",
        product_img: "https://icartadmin.irax.in/assets/img/uploads/products/facial1.webp",
        product_price: 23
    }]
}

export default data;