import React from "react";


const Cards = ({ imgSrc, productTitle, productPrice, productCurrency }) => {
    return (
        <div className="card">
            <img src={imgSrc} className="card-img-top" alt={productTitle} />
            <div className ="card-body">
            <h5 className ="card-title">{productTitle}</h5>
            <p className ="card-text"><strong>{productCurrency}</strong>{productPrice}</p>
            </div>
        </div>
    )
}

export default Cards;