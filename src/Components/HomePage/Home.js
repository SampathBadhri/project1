import React, { useState } from "react";

import Cards from '../Cards/Cards';
import Header from "../Header/Header";
import data from '../../data';

const Home = () => {
    const [exchangeRate, setExchangeRate] = useState(null)


    const currencyChange = (e) => {
        const currencyType = e.target.value;
        if (currencyType === "INR") {
            fetch(data.baseURL)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw response;
                }

            })
            .then(({ conversion_rates: { INR } }) => {
                setExchangeRate(INR)
            })
            .catch(e => {
                console.log(e);
            })
        } else{
            setExchangeRate(null);
        }
    }


    return (
        <section id="product-list">
            <div className="container">
                <Header title={"Products"}/>
                <div className="row">
                <div className="col-md-2 order-md-2 grid">
                        <label className="label">Currency</label>
                        <select className="bootstrap-select bg-secondary dropdown" onChange={currencyChange}>
                            <option value="USD" defaultValue>USD</option>
                            <option value="INR">INR</option>
                        </select>
                    </div>
                    <div className="col-md-10 order-md-1">
                        <div className="row">

                            {data.products.map((e,index) => {
                                return (
                                    <div className="col-lg-4 col-md-6 grid" key={e.id}>
                                        <Cards
                                            imgSrc={e.product_img}
                                            productTitle={e.product_title}
                                            productPrice={`${exchangeRate  ? (e.product_price * exchangeRate).toFixed(2) : e.product_price}`}
                                            productBrand={e.brand}
                                            productCurrency={`${exchangeRate ? '₹ ' : '$ '}`}
                                        />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    )
}

export default Home;